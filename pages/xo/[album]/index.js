import React, { useState } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import fetch from 'isomorphic-unfetch';
import Player from 'react-player';

function getDateString(time) {
  const date = new Date(time);

  const options = { year: 'numeric', month: 'long', day: 'numeric' };

  return date.toLocaleDateString('en-US', options);
}

function Header({
  title,
  released,
  src,
}) {
  return (
    <div className="row">
      <div className="col-lg-2 col-md-4 col-sm-6">
        <img src={src} alt={title} className="img-fluid rounded" />
      </div>
      <div className="col flex-column">
        <div className="h-25">
          <p className="text-monospace align-middle h-100">{released}</p>
        </div>
        <div className="h-75 d-flex align-items-center align-items-sm-center">
          <h1 className="display-3 font-weight-bold">{title}</h1>
        </div>
      </div>
    </div>
  );
}

function SongListItem({
  title,
  info,
  album,
  song,
  onClick,
}) {
  return (
    <button
      key={title}
      className="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-dark border-bottom"
      onClick={onClick}
    >
      <h3 className="text-white">{title}</h3>
      <span className="text-white">{info}</span>
    </button>
  );
}

function SongList({ list, onSongClick }) {
  const formatNumber = num => `${num}`.length > 1 ? `${num}` : `0${num}`;
  return (
    <div className="row justify-content-end">
    <style jsx global>{`
      .list-group-item {
        background-color: #000;
      }
    `}</style>
      <div className="list-group list-group-flush col-lg-10">
        {
          list.length > 0 && list.map(({
            name,
            length,
            album,
            songID,
          }) => {
            const minutes = Math.floor(length / 60);
            const seconds = formatNumber(length % 60);

            return (
              <SongListItem
                key={name}
                title={name}
                info={`${minutes}:${seconds}`}
                album={album}
                song={songID}
                onClick={() => onSongClick(songID)}
              />
            )
          })
        }
        {list.length === 0 && <h2 className="display-3 w-100 text-center p-5 mt-5 border">Songs Coming Soon!!!</h2>}
      </div>
    </div>
  )
}

function Modal({
  name: songName,
  youtube,
  onClose,
  album: {
    name: albumName,
  }
}) {
  return (
    <div
      id="song-modal"
      className="modal fade show"
      tabIndex="-1"
      role="dialog"
      style={{
        display: 'block',
        background: 'rgba(0, 0, 0, 0.5) !important',
      }}
    >
      <div className="modal-dialog modal-xl" role="document">
        <div className="modal-content">
          <div className="modal-header bg-dark">
            <h5 className="modal-title" id="exampleModalLiveLabel">{songName} - {albumName}</h5>
            <button type="button" className="close text-white" data-dismiss="modal" aria-label="Close" onClick={onClose}>
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div className="modal-body bg-dark">
            <Player
              url={youtube}
              width="100%"
              height="30rem"
              playing
            />
          </div>
        </div>
      </div>
    </div>
  );
}

function Album({
  albumData,
  albumSongs,
}) {
  if (albumData.error) return <div>Album Error</div>;
  if (albumData.notFound) return <div>Album Not Found</div>;
  // if (albumSongs.error) return <div>Song Error</div>;
  // if (albumSongs.notFound) return <div>Songs Not Found</div>;

  const [showModal, setShowModal] = useState(false);
  const [songDetails, setSongDetails] = useState({});
  const onSongClick = (id) => {
    const song = albumSongs.data.filter(s => s.songID === id)[0];
    if (!song) return console.error('Cannot find song!');

    setSongDetails(song);
    setShowModal(true);
  };

  const onModalClose = () => {
    setSongDetails({});
    setShowModal(false);
  };

  const {
    name,
    cover: {
      url: [imgSrc],
    },
    released,
  } = albumData;

  const {
    data: songList,
  } = albumSongs;

  const releasedOn = getDateString(released);

  return (
    <React.Fragment>
      <Head>
        <title>{name}</title>
      </Head>
      <div className="container mt-4 align-items-end">
        <Header src={imgSrc} title={name} released={releasedOn} />
        <SongList list={songList} onSongClick={onSongClick} />
        {showModal && <Modal {...songDetails} album={albumData} onClose={onModalClose} />}
      </div>
    </React.Fragment>
  );
};

Album.getInitialProps = async (context) => {
  const { song, album } = context.query;
  const options = {};

  const url = process.env.API_URL;
  let [albumData, albumSongs] = await Promise.all([
    fetch(`${url}/music/album?artist=theweeknd&album=${album}`)
      .then(res => res.json())
      .catch(() => ({ error: true })),
    fetch(`${url}/music/song/all?artist=theweeknd&album=${album}`)
      .then(res => res.json())
      .catch(() => ({ error: true, data: [] })),
  ]);

  if (!albumData.error && !albumData.name) {
    albumData = { notFound: true };
  }
  if (!albumSongs.error && !albumSongs.data.length === 0) {
    albumSongs = { notFound: true, data: [] };
  }
  // console.log(albumSongs);

  return {
    albumData,
    albumSongs,
  };
};

export default Album;
